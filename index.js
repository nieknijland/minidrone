const {DroneConnection, CommandParser} = require('minidrone-js');

const parser = new CommandParser();
const drone = new DroneConnection();


/* 
 * Commands are easily found by reading the xml specification
 * https://github.com/Parrot-Developers/arsdk-xml/blob/master/xml/
 */
const takeoff = parser.getCommand('minidrone', 'Piloting', 'TakeOff');
const landing = parser.getCommand('minidrone', 'Piloting', 'Landing');
const backFlip = parser.getCommand('minidrone', 'Animations', 'Flip', {direction: 'back'});

/** Helper function */
function sleep(ms) {
  return new Promise(a => setTimeout(a, ms));
}

drone.on('connected', async () => {
  // Makes the code a bit clearer
  const runCommand = x => drone.runCommand(x);

  await runCommand(takeoff);
  
  await sleep(2000);
  await runCommand(backFlip);
  
  await sleep(2000);
  await runCommand(landing);

  await sleep(5000);
  process.exit();
});
